#!/usr/bin/env python2.7

import collections
import heapq
import sys

# Build Graph

def read_graph(stream):
    '''
    >>> g = read_graph(['1 2 3', '1 3 4'])
    >>> g['1']['2'] == g['2']['1'] == 3
    True
    >>> g['1']['3'] == g['3']['1'] == 4
    True
    >>> g['1']['2'] == g['1']['3']
    False
    '''

    g = collections.defaultdict(dict)
    for line in stream:
        source, target, weight = line.split()
        g[source][target] = int(weight)
        g[target][source] = int(weight)
    return g

# Compute MST

def compute_mst(g):
    frontier   = []
    marked     = {}
    start      = g.keys()[0]

    heapq.heappush(frontier, (0, start, start))

    while frontier:
        weight, source, target = heapq.heappop(frontier)

        if target in marked:
            continue

        marked[target] = source

        for neighbor, cost in g[target].items():
            heapq.heappush(frontier, (cost, target, neighbor))

    return marked

# Organize Edges

def organize_edges(e):
    ''' 
    >>> organize_edges([(1, 1), (1, 2), (3, 2)])
    [(1, 2), (2, 3)]
    '''
    return sorted((min(s, t), max(s, t)) for s, t in e if s != t)

# Main Execution

if __name__ == '__main__':
    g = read_graph(sys.stdin)
    m = compute_mst(g)
    e = organize_edges(m.items())
    print sum(g[s][t] for s, t in e)
    for s, t in e:
        print s, t
