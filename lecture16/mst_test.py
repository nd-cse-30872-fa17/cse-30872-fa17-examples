#!/usr/bin/env python2.7

import unittest
import mst

class TestMST(unittest.TestCase):

    def test_read_graph(self):
        g = mst.read_graph(['1 2 3', '1 3 4'])
        self.assertTrue(g['1']['2'] == g['2']['1'] == 3)
        self.assertTrue(g['1']['3'] == g['3']['1'] == 4)
        self.assertFalse(g['1']['2'] == g['1']['3'])

    def test_organize_edges(self):
        self.assertEqual(
            mst.organize_edges([(1, 1), (1, 2), (3, 2)]),
            [(1, 2), (2, 3)]
        )

if __name__ == '__main__':
    unittest.main()
