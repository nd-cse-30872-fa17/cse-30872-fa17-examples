#!/bin/bash

test-input() {
    cat <<EOF
A B 1
A E 3
A D 4
B D 4
B E 2
C E 4
C F 5
D E 4
E F 7
EOF
}

test-output() {
    cat <<EOF
16
A B
A D
B E
C E
C F
EOF
}

if [ $# -lt 1 ]; then
    echo "Usage: $0 PROGRAM"
    exit 1
fi

PROGRAM=$1

echo -n "Testing $PROGRAM ... "
if diff -q <(test-input | $PROGRAM) <(test-output) 2>&1 > /dev/null; then
    echo "Success"
else
    echo "Failure"
fi
