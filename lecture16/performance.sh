#!/bin/bash

test-input() {
    cat <<EOF
A B 1
A E 3
A D 4
B D 4
B E 2
C E 4
C F 5
D E 4
E F 7
EOF
}

if [ $# -lt 1 ]; then
    echo "Usage: $0 PROGRAM"
    exit 1
fi

PROGRAM=$1

echo -n "Timing $PROGRAM ... "
#test-input | { time $PROGRAM 2>&1 > /dev/null; } |& awk '/real/ { print $2 }' 
if test-input | timeout 5 $PROGRAM > /dev/null 2>&1; then
    echo Success
else
    echo Failure
fi
