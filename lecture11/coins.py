#!/usr/bin/env python2.7

import sys

COINS = (1, 3, 4)

# Compute Coins Table

def compute_table(n, coins=COINS):
    # Initialize table to 0's
    table = [0] * (n + 1)

    # Initialize base cases (ie. coins)
    for coin in coins:
        table[coin] = 1

    # For each entry i in table, compute the following recurrence relation:
    #
    #   table[i] = min(table[i - coin] + 1 for coin in coins if (i - coin) >= 0)
    for i in range(1, n + 1):
        if table[i] != 0:
            continue

        table[i] = min(table[i - coin] + 1 for coin in coins if (i - coin) >= 0)

    return table

# Main execution

TABLE = compute_table(100)                  # Compute table

for n in map(int, sys.stdin):
    print '{} = {}'.format(n, TABLE[n])     # Lookup solutions in table
