#!/usr/bin/env python2.7

import collections
import sys

INFINITY = float('inf')

# Turtle Tuple

Turtle = collections.namedtuple('Turtle', 'weight strength capacity')

# Compute Weights

def compute_weights(turtles):
    # Insert sentinel/padding
    turtles.insert(0, None)

    # Initialize weights table:
    #
    #   Weights(i, j) = Minimum weight of selecting j turtles from first i turtles
    #
    # By default, we set Weights(i, j) to INFINITY
    weights = [[INFINITY for _ in range(len(turtles))] for _ in range(len(turtles))]

    # If we can select no turtles (j == 0), then minimum weight is 0
    for i in range(len(turtles)):
        weights[i][0] = 0

    # Construct table to minimize weight of selecting j turtles from the first
    # i turtles:
    #
    #   If Turtle(i)'s capacity >= Weights(i - 1, j - 1) Then
    #       Weights(i, j) = Min(Weights(i - 1, j), Weights(i - 1, j - 1) + Turtles[i].weight)
    #   Otherwise
    #       Weights(i, j) = Weights(i - 1, j)
    for i in range(1, len(turtles)):
        for j in range(1, i + 1):
            weights[i][j] = min(weights[i][j], weights[i - 1][j])
            if turtles[i].capacity >= weights[i - 1][j - 1]:
                weights[i][j] = min(weights[i][j],
                                    weights[i - 1][j - 1] + turtles[i].weight)
    return weights

# Count turtles

def determine_count(Turtles, Weights):
    count = len(Weights[-1]) - 1
    while count > 0 and Weights[-1][count] == INFINITY:
        count -= 1

    return count

# Main execution

Numbers = map(lambda line: map(int, line.split()), sys.stdin)
Turtles = map(lambda args: Turtle(args[0], args[1], args[1] - args[0]), Numbers)
Turtles = sorted(Turtles, key=lambda t: t.capacity)
Weights = compute_weights(Turtles)
Count   = determine_count(Turtles, Weights)
print Count
