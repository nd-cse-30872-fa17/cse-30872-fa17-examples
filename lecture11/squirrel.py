#!/usr/bin/env python2.7

# Notes:
#
#   1. Each S(r, c) contains the optimal solution for that square.
#
#   2. Final solution is in the final corner: S(n, n).
#
#   3. Building the table does not reveal what the path is, just what the
#   maximum squirrel population is.

import sys

def read_grid(n):
    # Pad grid
    grid = [[0 for _ in range(n + 1)]]
    for _ in range(n):
        grid.append([0] + map(int, sys.stdin.readline().split()))

    return grid

def hunt_squirrels(grid, n):
    # 1. Initialize table
    table = [[0 for _ in range(n + 1)] for _ in range(n + 1)]

    # 2. Table[row][col] = Grid[row][col] + max(from_left, from_above)
    #
    #   S(r, c) = Max(S(r, c - 1), S(r - 1, c)) + G(r, c)
    #
    for row in range(1, n + 1):
        for col in range(1, n + 1):
            table[row][col] = grid[row][col] + max(
                table[row][col - 1],
                table[row - 1][col]
            )

    # 3. Use table result
    return table[n][n]

# Main execution

while True:
    try:
        n = int(sys.stdin.readline())
    except ValueError:
        break

    grid = read_grid(n)
    print hunt_squirrels(grid, n)
