/* first_nonrepeat.c */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char find_first_nonpeating(char *s) {
    int counts[1<<8] = {0};

    /* Count occurrences of each letter */
    for (char *c = s; *c; c++)
	counts[(int)(tolower(*c))]++;

    /* Check occurrences of each letter */
    for (char *c = s; *c; c++)
	if (counts[(int)(tolower(*c))] == 1)
	    return *c;
}

int main(int argc, char *argv[]) {
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stdin)) {
    	printf("%c\n", find_first_nonpeating(buffer));
    }

    return 0;
}
