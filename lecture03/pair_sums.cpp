// pair_sums.cpp

#include <iostream>
#include <vector>

using namespace std;

#define NMAX	10000

void read_vector(vector<int> &v, int n) {
    int t;
    for (int i = 0; i < n; i++) {
    	cin >> t;
    	v.push_back(t);
    }
}

void find_pair_sums(vector<int> &v, int k) {
    int table[NMAX] = {0};

    for (auto i : v) {
    	int diff = k - i;
    	if (diff >= 0 && table[diff] > 0)
    	    printf("%d = %d + %d\n", k, i, diff);
    	table[i]++;
    }
}

int main(int argc, char *argv[]) {
    int n, k;

    while (cin >> n) {
    	vector<int> numbers;

    	read_vector(numbers, n);
    	cin >> k;
    	find_pair_sums(numbers, k);
    }

    return 0;
}
