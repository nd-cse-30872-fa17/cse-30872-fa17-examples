#!/usr/bin/env python2.7

import sys

data = []
for line in sys.stdin:
    number = int(line)
    if number < 0:
        break

    data.append(number)

print data
