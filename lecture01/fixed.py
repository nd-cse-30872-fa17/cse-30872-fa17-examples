#!/usr/bin/env python2.7

import sys

data  = []
limit = int(sys.stdin.readline())

for _ in range(limit):
    n = int(sys.stdin.readline())
    data.append(n)

print data
