#!/usr/bin/env python2.7

import sys

''' Counting Solution '''
def is_palindromic(s):
    counts = {}
    for c in s:
        counts[c] = counts.get(c, 0) + 1

    odds = 0
    for count in counts.values():
        odds += count % 2

    return odds <= 1

''' History Solution '''
def is_palindromic_set(s):
    seen = set()
    for c in s:
        if c in seen:
            seen.remove(c)
        else:
            seen.add(c)
    return len(seen) <= 1

for word in sys.stdin:
    print 'Yes' if is_palindromic(word.rstrip()) else 'No'
