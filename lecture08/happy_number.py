#!/usr/bin/env python2.7

import sys

''' Naive version '''
def is_happy(n, seen):
    if n in seen:
        return False

    if n == 1:
        return True

    seen.add(n)
    return is_happy(sum(x * x for x in map(int, str(n))), seen)

''' Memoized version '''
HAPPY_CACHE = {}

def is_happy_cached(n, seen):
    if n in seen:
        return False

    if n == 1:
        return True
    
    if n not in HAPPY_CACHE:
        seen.add(n)
        HAPPY_CACHE[n] = is_happy_cached(sum(x * x for x in map(int, str(n))), seen)

    return HAPPY_CACHE[n]


for n in map(int, sys.stdin):
    print 'Yes' if is_happy_cached(n, set()) else 'No'
