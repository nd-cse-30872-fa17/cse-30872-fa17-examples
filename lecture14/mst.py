#!/usr/bin/env python2.7

import collections
import heapq
import sys

# Build Graph

def read_graph():
    g = collections.defaultdict(dict)
    for line in sys.stdin:
        source, target, weight = line.split()
        g[source][target] = int(weight)
        g[target][source] = int(weight)
    return g

# Compute MST

def compute_mst(g):
    frontier   = []
    marked     = {}
    start      = g.keys()[0]

    heapq.heappush(frontier, (0, start, start))

    while frontier:
        weight, source, target = heapq.heappop(frontier)

        if target in marked:
            continue

        marked[target] = source

        for neighbor, cost in g[target].items():
            heapq.heappush(frontier, (cost, target, neighbor))

    return marked

# Main Execution

if __name__ == '__main__':
    g = read_graph()
    m = compute_mst(g)
    e = sorted((min(s, t), max(s, t)) for s, t in m.items() if s != t)
    print sum(g[s][t] for s, t in e)
    for s, t in e:
        print s, t
