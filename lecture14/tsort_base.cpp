// tsort.cpp: Topological Sort Makefile

#include <cstdlib>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

// Trim Utility ----------------------------------------------------------------

string trim(const string &s) {
    string r(s);
    try {
	r.erase(0, s.find_first_not_of(" \n\r\t"));
    } catch (out_of_range) {
    	;
    }
    try {
    	r.erase(s.find_last_not_of(" \n\r\t")+1);
    } catch (out_of_range) {
    	;
    }
    return r;
}

// Graph Structure -------------------------------------------------------------

struct Graph {
    map<string, set<string>>	edges;
    map<string, size_t>		degrees;

    void load_graph(istream &is) {
    	// TODO
    }

    void topological_sort(vector<string> &sorted) {
    	// TODO
    }
};

// Main Execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    Graph g;
    vector<string> sorted;

    g.load_graph(cin);
    g.topological_sort(sorted);

    if (sorted.size() != g.edges.size()) {
	cerr << "Graph contains a cycle!" << endl;
	return EXIT_FAILURE;
    }

    for (auto &v : sorted) {
	cout << v << endl;
    }

    return EXIT_SUCCESS;
}
