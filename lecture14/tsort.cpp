// tsort.cpp: Topological Sort Makefile

#include <cstdlib>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

// Trim Utility ----------------------------------------------------------------

string trim(const string &s) {
    string r(s);
    try {
	r.erase(0, s.find_first_not_of(" \n\r\t"));
    } catch (out_of_range) {
    	;
    }
    try {
    	r.erase(s.find_last_not_of(" \n\r\t")+1);
    } catch (out_of_range) {
    	;
    }
    return r;
}

// Graph Structure -------------------------------------------------------------

struct Graph {
    map<string, set<string>>	edges;
    map<string, size_t>		degrees;

    void load_graph(istream &is) {
    	string line;

    	while (getline(is, line)) {
    	    size_t split = line.find(':');
    	    if (split == string::npos)
    	    	continue;

    	    string target  = trim(line.substr(0, split));
    	    string sources = trim(line.substr(split + 1));
    	    	
    	    stringstream ss(sources);
    	    string source;
    	    while (ss >> source) {
		if (!degrees.count(source))
		    degrees[source] = 0;

    	    	edges[source].insert(target);
    	    	degrees[target] += 1;
	    }
	}
    }

    void topological_sort(vector<string> &sorted) {
    	queue<string> frontier;

    	for (auto &v : degrees)
    	    if (v.second == 0)
    	    	frontier.push(v.first);

	while (!frontier.empty()) {
	    auto n = frontier.front(); frontier.pop();

	    sorted.push_back(n);

	    for (auto &v : edges[n]) {
	    	degrees[v] -= 1;
	    	if (degrees[v] == 0)
	    	    frontier.push(v);
	    }
	}
    }
};

// Main Execution --------------------------------------------------------------

int main(int argc, char *argv[]) {
    Graph g;
    vector<string> sorted;

    g.load_graph(cin);
    g.topological_sort(sorted);

    if (sorted.size() != g.edges.size()) {
	cerr << "Graph contains a cycle!" << endl;
	return EXIT_FAILURE;
    }

    for (auto &v : sorted) {
	cout << v << endl;
    }

    return EXIT_SUCCESS;
}
