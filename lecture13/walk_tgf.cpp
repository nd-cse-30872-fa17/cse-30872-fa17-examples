// walk_tgf.cpp: read TGF and walk it

#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

// WalkType Constants

typedef enum {
    DFS_REC,
    DFS_ITER,
    BFS_ITER,
} WalkType;

// Graph structure

template <typename NodeLabel, typename EdgeLabel>
struct Graph {
    vector <NodeLabel> labels;
    vector <set<int>>  edges;
};

// Load graph from standard input

template <typename NodeLabel, typename EdgeLabel>
void load_graph(Graph<NodeLabel, EdgeLabel> &g) {
    string line;

    // TODO: Read labels (vertices)

    // TODO: Read edges (vertices)
}

// Walk graph dispatch function

template <typename NodeLabel, typename EdgeLabel>
void walk_graph(Graph<NodeLabel, EdgeLabel> &g, int root, WalkType w) {
    set<int> marked;

    switch (w) {
        case DFS_REC:
            walk_graph_dfs_rec(g, root, marked);
            break;
        case DFS_ITER:
            walk_graph_dfs_iter(g, root);
            break;
        case BFS_ITER:
            walk_graph_bfs_iter(g, root);
            break;
        default:
            cerr << "Unknown WalkType: " << w << endl;
            break;
    }
}

// Depth-First-Search (recursive)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_dfs_rec(Graph<NodeLabel, EdgeLabel> &g, int v, set<int> &marked) {
    // TODO
}

// Depth-First-Search (iterative)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_dfs_iter(Graph<NodeLabel, EdgeLabel> &g, int v) {
    // TODO
}

// Breadth-First-Search (iterative)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_bfs_iter(Graph<NodeLabel, EdgeLabel> &g, int v) {
    // TODO
}

// Main execution

int main(int argc, char *argv[]) {
    Graph<string, string> g;

    if (argc != 3) {
        cerr << "Usage: " << argv[0] << " root [0 = DFS_REC | 1 = DFS_ITER | 2 = BFS_ITER]" << endl;
        return EXIT_FAILURE;
    }

    int root = atoi(argv[1]);
    int walk = atoi(argv[2]);

    load_graph(g);
    walk_graph(g, root, static_cast<WalkType>(walk));

    return EXIT_SUCCESS;
};
