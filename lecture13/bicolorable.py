#!/usr/bin/env python2.7

import collections
import sys

# Constants

BLUE = 0
RED  = 1

# Read Graph

def read_graph(n, m):
    g = {v: set() for v in range(n)}

    for _ in range(m):
        s, t = map(int, sys.stdin.readline().split())
        g[s].add(t)
        g[t].add(s)

    return g

# Determine if Bicolorable

def is_bicolorable(g):
    return walk(g, g.keys()[0], BLUE, {})

def walk(g, n, color, visited):
    if n in visited:
        return visited[n] == color

    visited[n] = color
    for v in g[n]:
        if not walk(g, v, (color + 1) % 2, visited):
            return False

    return True

# Main execution

if __name__ == '__main__':
    n, m = map(int, sys.stdin.readline().split())
    while n and m:
        g = read_graph(n, m)
        if is_bicolorable(g):
            print 'BICOLORABLE'
        else:
            print 'NOT BICOLORABLE'

        n, m = map(int, sys.stdin.readline().split())
