#!/usr/bin/env python2.7

import sys

def compare_numbers(a, b):
    ab = int(a + b)
    ba = int(b + a)
    return ab - ba

for line in sys.stdin:
    print ''.join(sorted(line.split(), cmp=compare_numbers, reverse=True))
