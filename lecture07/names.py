#!/usr/bin/env python2.7

import sys

names = []

for line in sys.stdin:
    names.append(line.split())

'''
names = sorted(names, key=lambda p: p[0])
names = sorted(names, key=lambda p: p[1])
'''

names = sorted(names, lambda a, b: cmp(a[0], b[0]) if a[1] == b[1] else cmp(a[1], b[1]))

for name in names:
    print '{} {}'.format(*name)
