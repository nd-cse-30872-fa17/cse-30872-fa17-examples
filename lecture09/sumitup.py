#!/usr/bin/env python2.7

import itertools
import sys

for line in sys.stdin:
    data    = map(int, line.split())
    target  = data[0]
    n       = data[1]
    numbers = data[2:]
    count   = 0
    results = {}

    if target == 0:
        break

    print 'Sums of {}:'.format(target)

    for length in range(1, n + 1):
        for combination in itertools.combinations(numbers, length):
            if sum(combination) == target and combination not in results:
                results[combination] = count
                count += 1

    for result in sorted(results, key=results.get):
        print '+'.join(map(str, result))

    if count == 0:
        print 'NONE'
