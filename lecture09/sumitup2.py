#!/usr/bin/env python2.7

import itertools
import sys

def sumitup(index, subset, numbers, target, results):
    # Base case: subset sums up to target, so store it
    if sum(subset) == target:
        result = tuple(subset)  # Create copy

        if not results or result != results[-1]:
            results.append(result)
        return

    # Base case: prune search when index is length of numbers, or sum of susbet
    # is greater than target
    if index == len(numbers) or sum(subset) > target:
        return

    # Recursive step: try with current number and without
    subset.append(numbers[index])   # Attempt with number
    sumitup(index + 1, subset, numbers, target, results)
    subset.pop(-1)                  # Attempt without number
    sumitup(index + 1, subset, numbers, target, results)


for line in sys.stdin:
    data    = map(int, line.split())
    target  = data[0]
    n       = data[1]
    numbers = data[2:]
    results = []

    if target == 0:
        break

    print 'Sums of {}:'.format(target)

    sumitup(0, [], numbers, target, results)

    #for result in sorted(results, key=results.get):
    for result in results:
        print '+'.join(map(str, result))

    if not results:
        print 'NONE'
