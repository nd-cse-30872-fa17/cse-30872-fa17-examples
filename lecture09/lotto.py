#!/usr/bin/env python2.7

import itertools
import sys

LOTTO_NUMBERS = 6

for line, numbers in enumerate(map(str.split, sys.stdin)):
    if len(numbers) <= 1:
        break
    
    if line:
        print ''

    numbers = map(int, numbers[1:])
    for combination in sorted(itertools.combinations(numbers, LOTTO_NUMBERS)):
        print ' '.join(map(str, combination))
