#include <iostream>
#include <set>
#include <vector>

using namespace std;

/*
const size_t n = 3;
vector<int> permutation;
bool chosen[n] = {false};

void search() {
    if (permutation.size() == n) {
    } else {
    	for (size_t i = 0; i < n; i++) {
    	    if (chosen[i]) continue;
    	    chosen[i] = true;
    	    permutation.push_back(i);
    	    search();
    	    chosen[i] = false;
    	    permutation.pop_back();
	}
    }
}
*/

void permutations(vector<int> &p, set<int> &c, int r) {
    // Display permutation at each function call
    for (size_t i = 0; i < r; i++) cout << " ";

    for (auto i : p) { cout << i << " "; }; cout << endl;

    if (c.empty()) {	// We have used all candidates, so we are done
    	return;
    }

    for (auto e : c) {
	p.push_back(e);	// Place one element into current permutation
	c.erase(e);	// Remove element from candiate list

	// Recurse on sublist (which now is one item shorter and no longer
	// includes the previously placed element as a candidate
	permutations(p, c, r + 1);

	c.insert(e);	// Add element back to candidate set
	p.pop_back();	// Remove element from current permutation
    }
}

int main(int argc, char *argv[]) {
    vector<int> p = {};
    set<int>	c = {0, 1, 2};
    int		r = 0;

    permutations(p, c, r);
    return 0;
}
