// subsets2.cpp

#include <iostream>
#include <numeric>
#include <vector>

using std::accumulate;
using std::cout;
using std::endl;
using std::vector;

int main(int argc, char *argv[]) {
    vector<int> numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    size_t      count   = 0;

    for (size_t b = 0; b < (1<<numbers.size()); b++) {
	vector<int> subset;
	for (size_t i = 0; i < numbers.size(); i++) {
	    if (b & (1<<i)) subset.push_back(numbers[i]);
	}

	auto sum = accumulate(subset.begin(), subset.end(), 0);
	count   += (sum % 3 == 0) ? 1 : 0;
    }

    cout << count << endl;
    return 0;
}
