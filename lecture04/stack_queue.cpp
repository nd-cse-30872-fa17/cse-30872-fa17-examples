// stack_queue.cpp

#include <iostream>
#include <stack>

using namespace std;

template <typename T>
class stack_queue {
public:
    bool empty() const { return in.empty() && out.empty(); }

    const T& front() {
	if (out.empty()) {
	    in_to_out();
	}
	return out.top();
    }

    const T& back() const { return in.top(); }

    void push(const T& value) { in.push(value); }

    void pop() {
	if (out.empty()) {
	    in_to_out();
	}
	out.pop();
    }

private:
    stack<T> in, out;

    void in_to_out() {
    	while (!in.empty()) {
    	    out.push(in.top());
    	    in.pop();
	}
    }
};

int main(int argc, char *argv[]) {
    stack_queue <int> s;

    s.push(2);
    cout << s.front() << endl;
    s.push(3);
    cout << s.front() << endl;
    s.push(1);
    cout << s.front() << endl;
    s.pop();
    cout << s.front() << endl;
    s.pop();
    cout << s.front() << endl;

    return 0;
}
