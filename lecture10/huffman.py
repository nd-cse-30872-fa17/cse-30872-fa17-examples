#!/usr/bin/env python2.7

import collections
import heapq
import sys

class Node(object):
    def __init__(self, name, value, left=None, right=None):
        self.left  = left
        self.right = right
        self.name  = name
        self.value = value

        if self.left and self.right:
            self.name  = self.left.name  + self.right.name
            self.value = self.left.value + self.right.value

    def walk(self, codeword, results):
        if self.left:
            self.left.walk(codeword + '0', results)

        if self.right:
            self.right.walk(codeword + '1', results)

        if not self.left and not self.right:
            results[self.name] = codeword

    def __cmp__(self, other):
        if self.value == other.value:
            return cmp(len(self.name), len(other.name))
        return cmp(self.value, other.value)

    def __str__(self):
        return 'name={}, value={}'.format(self.name, self.value)

for line in sys.stdin:
    counter = collections.Counter(line.strip())
    nodes   = map(lambda p: Node(*p), counter.items())

    heapq.heapify(nodes)
    while len(nodes) > 1:
        left  = heapq.heappop(nodes)
        right = heapq.heappop(nodes)
        node  = Node(None, 0, left, right)
        heapq.heappush(nodes, node)

    results = {}
    nodes[0].walk('', results)
    print ', '.join('{} = {}'.format(k, v) for k, v in sorted(results.items()))
