#!/usr/bin/env python2.7

import sys

COINS = (25, 10, 5, 1)

def make_change(n):
    change = []
    for coin in COINS:
        while n >= coin:
            change.append(coin)
            n -= coin
    return change

for target in map(int, sys.stdin):
    print ' + '.join(map(str, make_change(target)))

