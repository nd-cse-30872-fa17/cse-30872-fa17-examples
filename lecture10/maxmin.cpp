// challenge 09: Min Max

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    int n, k, t, u, d;

    while (cin >> n >> k) {
    	vector<int> v;

	// Read in input
    	for (int i = 0; i < n; i++) {
    	    cin >> t;
    	    v.push_back(t);
	}

	// Sort in ascending order
	sort(v.begin(), v.end());
	u = v[n - 1] - v[0];		// Initial unfairness
	for (int i = 0; i <= (n - k); i++) {
	    d = v[i + k - 1] - v[i];	// Difference
	    u = min(d, u);		// Update unfairness
	}

	cout << u << endl;
    }

    return 0;
}
