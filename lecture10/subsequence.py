#!/usr/bin/env python2.7

import sys

# Is Subsequence

def is_subsequence(s, t):
    sindex = 0
    tindex = 0

    while sindex < len(s) and tindex < len(t):
        if s[sindex] == t[tindex]:
            sindex += 1
            
        tindex += 1

    return sindex >= len(s)

# Main Execution

for line in sys.stdin:
    s, t = line.split()
    print is_subsequence(s, t)
