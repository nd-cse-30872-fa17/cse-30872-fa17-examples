#!/usr/bin/env python2.7

import sys

# Assign cookies

def assign_cookies(children, cookies):
    count = 0

    while cookies and children:
        child  = children.pop(0)
        cookie = cookies[0]

        if child <= cookie:
            cookies.pop(0)
            count += 1

    return count

# Main execution

children = sys.stdin.readline().split()
cookies  = sys.stdin.readline().split()
while children and cookies:
    children = sorted(map(int, children), reverse=True)
    cookies  = sorted(map(int, cookies) , reverse=True)

    print assign_cookies(children, cookies)

    children = sys.stdin.readline().split()
    cookies  = sys.stdin.readline().split()
