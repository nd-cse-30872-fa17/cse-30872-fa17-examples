#!/usr/bin/env python2.7

import collections
import sys

# Read Graph

def read_graph():
    graph = collections.defaultdict(dict)
    edge  = 0       # Edge ordinal (trick from jbeiter)

    for line in sys.stdin:
        s, t = map(int, line.split())
        graph[s][t] = edge
        graph[t][s] = edge
        edge += 1

    return graph

# Find Circuit

def find_circuit(graph, start, vertex, visited, path):
    # If we have returned to start, return path
    if path and start == vertex:
        return path

    # Visit each unvisited outgoing edge
    for neighbor in graph[vertex]:
        if graph[vertex][neighbor] in visited:
            continue

        visited.add(graph[vertex][neighbor])
        path.append((vertex, neighbor))

        if find_circuit(graph, start, neighbor, visited, path):
            return path

        path.pop(-1)
        visited.remove(graph[vertex][neighbor])

    # No circuit found, so return nothing (should never happen!)
    return []

# Find Eulerian Circuit

def find_euler_circuit(graph):
    start    = graph.keys()[0]  # Startin vertex
    visited  = set()            # Visited edges (set of edge ordinals)
    circuit  = []               # Eulerian circuit (list of edges)
    index    = 0                # Where in circuit to insert subcircuit

    while start:
        # Find subcircuit and insert it after current component
        path    = find_circuit(graph, start, start, visited, [])
        circuit = circuit[0:index] + path + circuit[index:]

        # Check if any nodes in current circuit have an unused edge, if so, set
        # start so we search for subcircuit beginning at that vertex
        start = None
        for index, v in enumerate(s for s, t in circuit):
            for n, e in graph[v].items():
                if e not in visited:
                    start = v
                    break

    return circuit

# Main Execution

if __name__ == '__main__':
    graph   = read_graph()
    circuit = find_euler_circuit(graph)

    for s, t in circuit:
        print s, t
