// perfect_square.cpp: Perfect Square

#include <iostream>

using namespace std;

bool is_perfect_square(long n) {
    long low  = 1;
    long high = n;

    while (low <= high) {
        long middle = (low + high) / 2;

        if (middle * middle > n)
            high = middle - 1;
	else if (middle * middle < n)
            low  = middle + 1;
        else
            return true;
    }

    return false;
}

/*
bool is_perfect_square(long n) {
    for (long i = 0; i < n; i++)
    	if (i*i == n)
    	    return true;
    return false;
}
*/

int main(int argc, char *argv[]) {
    long n;

    while (cin >> n) {
    	if (is_perfect_square(n))
    	    cout << "Yes" << endl;
	else
    	    cout << "No" << endl;
    }

    return 0;
}
