#!/usr/bin/env python2.7

# Perfect Square

import sys

def is_perfect_square(n):
    ''' Binary search '''
    low, high = 1, n

    while low <= high:
        middle = (low + high) / 2

        if middle * middle > n:
            high = middle - 1
        elif middle * middle < n:
            low  = middle + 1
        else:
            return True

    return False

'''
Linear search
def is_perfect_square(n):
    for i in range(1, n):
        if i*i == n:
            return True
    return False
'''

for n in sys.stdin:
    if is_perfect_square(int(n)):
        print 'Yes'
    else:
        print 'No'
